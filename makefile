#Makefile for "geometrica-lab01" C++ application
#Created by Pedro Emerick and Valmir Correa 11/03/2017

BUILD_DIR = ./build
SRC_DIR = ./src
INCLUDE_DIR = ./include
BIN_DIR = ./bin
TEST_DIR = ./test
DOC_DIR = ./doc

RM = rm -rf

.PHONY: all clean distclean doxy

PROG = geometrica
CC = g++
CPPFLAGS = -O0 -g -Wall -pedantic -ansi -std=c++11 -I$(INCLUDE_DIR)
OBJS = main.o leitura.o area.o calcarea.o perimetro.o calcperimetro.o volume.o calcvolume.o 

$(PROG): $(OBJS)
	$(CC) -o  $(PROG) $(BUILD_DIR)/*.o -o $(BIN_DIR)/$(PROG)

main.o:
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/main.cpp -o $(BUILD_DIR)/main.o

leitura.o: $(INCLUDE_DIR)/leitura.h
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/leitura.cpp -o $(BUILD_DIR)/leitura.o

area.o: $(INCLUDE_DIR)/area.h
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/area.cpp -o $(BUILD_DIR)/area.o

calcarea.o: $(INCLUDE_DIR)/calcarea.h
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/calcarea.cpp -o $(BUILD_DIR)/calcarea.o

perimetro.o: $(INCLUDE_DIR)/perimetro.h
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/perimetro.cpp -o $(BUILD_DIR)/perimetro.o

calcperimetro.o: $(INCLUDE_DIR)/calcperimetro.h
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/calcperimetro.cpp -o $(BUILD_DIR)/calcperimetro.o

volume.o: $(INCLUDE_DIR)/volume.h
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/volume.cpp -o $(BUILD_DIR)/volume.o

calcvolume.o: $(INCLUDE_DIR)/calcvolume.h
	$(CC) $(CPPFLAGS) -c $(SRC_DIR)/calcvolume.cpp -o $(BUILD_DIR)/calcvolume.o

doxy:
	#$(RM) $(DOC_DIR)/*
	doxygen $(DOC_DIR)/Doxyfile

clean:
	$(RM) $(BIN_DIR)/* 
	$(RM) $(BUILD_DIR)/*

	
