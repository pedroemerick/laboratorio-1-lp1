/**
* @file calcvolume.cpp
* @brief Arquivo com as funções para mostrar o resultado do volume das figuras.
* @author Valmir Correa
* @since 09/03/17
* @date 11/03/17
*/
#include <iostream>
#include "calcvolume.h"
#include "leitura.h"

using std::endl;
using std::cout;


/**
* @brief Faz a chamada da função para o cálculo do volume do pirâmide e imprime o resultado.
* @param area_base Recebe o tamanho da base da pirâmide.
* @param altura Recebe o tamanho da altura da pirâmide.
*/
void CalcVolumePiramide (float area_base, float altura)
{
    cout << "Volume da Pirâmide: " << VolumePiramide (area_base, altura) << endl << endl; 

    Leitura ();
}

/**
* @brief Faz a chamada da função para o cálculo do volume do cubo e imprime o resultado.
* @param aresta Recebe o tamanho de uma das arestas do cubo.
*/
void CalcVolumeCubo (float aresta)
{
    cout << "Volume do cubo: " << VolumeCubo (aresta) << endl << endl; 

    Leitura ();
}

/**
* @brief Faz a chamada da função para o cálculo do volume do paralelepípedo e imprime o resultado.
* @param aresta1 Recebe o tamanho de uma das arestas dos diferentes pares do paralelepípedo.
* @param aresta2 Recebe o tamanho de uma das arestas dos diferentes pares do paralelepípedo.
* @param aresta3 Recebe o tamanho de uma das arestas dos diferentes pares do paralelepípedo.
*/
void CalcVolumeParalelepipedo (float aresta1, float aresta2, float aresta3)
{
    cout << "Volume do paralelepípedo: " << VolumeParalelepipedo (aresta1, aresta2, aresta3) << endl << endl; 

    Leitura ();
}

/**
* @brief Faz a chamada da função para o cálculo do volume da esfera e imprime o resultado.
* @param raio Recebe o tamanho do raio da esfera.
*/
void CalcVolumeEsfera (float raio)
{
    cout << "Volume da esfera: " << VolumeEsfera (raio) << endl << endl; 

    Leitura ();
}