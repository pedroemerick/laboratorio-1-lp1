/**
* @file area.cpp
* @brief Arquivo com as funções para o cálculo da área das figuras.
* @author Pedro Emerick
* @since 09/03/17
* @date 11/03/17
*/
#include <cmath>
#include "area.h"

/**
* @brief Função que calcula a área de um triângulo equilátero.
* @param base Recebe o tamanho da base do triângulo.
* @param altura Recebe o tamanho da altura do triângulo.
* @return Área do triângulo.
*/
float AreaTriangulo (float base, float altura)
{
    return (base*altura)/2;
}

/**
* @brief Função que calcula a área de um retângulo.
* @param base Recebe o tamanho da base do retângulo.
* @param altura Recebe o tamanho da altura do retângulo.
* @return Area do retângulo.
*/
float AreaRetangulo (float base, float altura)
{
    return base*altura;
}

/**
* @brief Função que calcula a área de um quadrado.
* @param lado Recebe o tamanho do lado do quadrado.
* @return Area do quadrado.
*/
float AreaQuadrado (float lado)
{
    return pow(lado,2);
}

/**
* @brief Função que calcula a área de um circulo.
* @param raio Recebe o tamanho do raio do circulo.
* @return Área do circulo.
*/
float AreaCirculo (float raio)
{
    return 3.1415 * pow(raio,2);
}

/**
* @brief Função que calcula a área de uma pirâmide.
* @param area_base Recebe o tamanho da área da base da pirâmide.
* @param area_lateral Recebe o tamanho de todas as áreas laterais somadas da pirâmide.
* @return Area da pirâmide.
*/
float AreaPiramide (float area_base, float area_lateral)
{
    return area_base + area_lateral;
}

/**
* @brief Função que calcula a área de um cubo.
* @param aresta Recebe o tamanho da aresta do cubo.
* @return Área do cubo.
*/
float AreaCubo (float aresta)
{
    return 6 * pow(aresta,2);
}

/**
* @brief Função que calcula a área de um paralelepípedo.
* @param aresta1 Recebe o tamanho de uma das arestas do paralelepípedo.
* @param aresta2 Recebe o tamanho de uma das arestas do paralelepípedo.
* @param aresta3 Recebe o tamanho de uma das arestas do paralelepípedo.
* @return Área do paralelepípedo.
*/
float AreaParalelepipedo (float aresta1, float aresta2, float aresta3)
{
    return (2*aresta1*aresta2) + (2*aresta1*aresta3) + (2*aresta2*aresta3);
}

/**
* @brief Função que calcula a área de uma esfera.
* @param raio Recebe o tamanho do raio da esfera.
* @return Área da esfera.
*/
float AreaEsfera (float raio)
{
    return 4 * 3.1415 * pow(raio,2);
}