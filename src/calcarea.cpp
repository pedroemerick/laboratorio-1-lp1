/**
* @file calcarea.cpp
* @brief Arquivo com as funções para mostrar o resultado da área das figuras.
* @author Pedro Emerick
* @since 09/03/17
* @date 11/03/17
*/
#include <iostream>
#include "calcarea.h"

using std::endl;
using std::cout;

/**
* @brief Faz a chamada da função para o cálculo da área do triângulo e imprime o resultado.
* @param base Recebe o tamanho da base do triângulo.
* @param altura Recebe o tamanho da altura do triângulo.
*/
void CalcAreaTriangulo (float base, float altura)
{
    cout << "\nArea do triangulo: " << AreaTriangulo (base, altura) << endl;
}

/**
* @brief Faz a chamada da função para o cálculo da área do retângulo e imprime o resultado.
* @param base Recebe o tamanho da base do retângulo.
* @param altura Recebe o tamanho da altura do retângulo.
*/
void CalcAreaRetangulo (float base, float altura)
{
    cout << "\nArea do retangulo: " << AreaRetangulo (base, altura) << endl;
}

/**
* @brief Faz a chamada da função para o cálculo da área do quadrado e imprime o resultado.
* @param lado Recebe o tamanho do lado do quadrado.
*/
void CalcAreaQuadrado (float lado)
{
    cout << "\nArea do quadrado: " << AreaQuadrado (lado) << endl;
}

/**
* @brief Faz a chamada da função para o cálculo da área do circulo e imprime o resultado.
* @param raio Recebe o tamanho do raio do circulo.
*/
void CalcAreaCirculo (float raio)
{
    cout << "\nArea do circulo: " << AreaCirculo (raio) << endl;
}

/**
* @brief Faz a chamada da função para o cálculo da área da pirâmide e imprime o resultado.
* @param area_base Recebe o tamanho da área da base da pirâmide.
* @param area_lateral Recebe o tamanho de todas as áreas laterais somadas da pirâmide.
*/
void CalcAreaPiramide (float area_base, float area_lateral)
{
    cout << "\nArea da piramide: " << AreaPiramide (area_base, area_lateral) << endl;
}

/**
* @brief Faz a chamada da função para o cálculo da área do cubo e imprime o resultado.
* @param aresta Recebe o tamanho da aresta do cubo.
*/
void CalcAreaCubo (float aresta)
{
    cout << "\nArea do cubo: " << AreaCubo (aresta) << endl;
}

/**
* @brief Faz a chamada da função para o cálculo da área do paralelepípedo e imprime o resultado.
* @param aresta1 Recebe o tamanho de uma das arestas do paralelepípedo.
* @param aresta2 Recebe o tamanho de uma das arestas do paralelepípedo.
* @param aresta3 Recebe o tamanho de uma das arestas do paralelepípedo.
*/
void CalcAreaParalelepipedo (float aresta1, float aresta2, float aresta3)
{
    cout << "\nArea do paralelepipedo: " << AreaParalelepipedo (aresta1, aresta2, aresta3) << endl;
}

/**
* @brief Faz a chamada da função para o cálculo da área da esfera e imprime o resultado.
* @param raio Recebe o tamanho do raio da esfera.
*/
void CalcAreaEsfera (float raio)
{
    cout << "\nArea da esfera: " << AreaEsfera (raio) << endl;
}