/**
* @file perimetro.cpp
* @brief Arquivo com as funções para o cálculo do perímetro das figuras.
* @author Valmir Correa
* @since 09/03/17
* @date 11/03/17
*/

#include "perimetro.h"

/**
* @brief Realiza o cálculo do perímetro do triângulo equilátero.
* @param lado Recebe o tamanho de um dos lados do triângulo.
* @return Perímetro do triângulo.
*/
float PerimetroTriangulo (float lado) {

    return (3 * lado);
}

/**
* @brief Realiza o cálculo do perímetro do retângulo.
* @param base Recebe o tamanho da base do retângulo.
* @param altura Recebe o tamanho da altura do retângulo.
* @return Perímetro do retângulo.
*/
float PerimetroRetangulo (float base, float altura) {
    
    return (2 * ( base + altura ) );
}

/**
* @brief Realiza o cálculo do perímetro do quadrado.
* @param lado Recebe o tamanho do lado do quadrado.
* @return Perímetro do quadrado.
*/
float PerimetroQuadrado (float lado) {
    
    return (4 * lado);
}

/**
* @brief Realiza o cáculo do perímetro do círculo. 
* @param raio Recebe o tamanho do raio do círculo.
* @return Perímetro do círculo.
*/
float PerimetroCirculo (float raio) {
       
    return (2 * raio * 3.1415 );
}