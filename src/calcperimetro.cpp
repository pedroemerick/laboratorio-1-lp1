/**
* @file calcperimetro.cpp
* @brief Arquivo com as funções para mostrar o resultado do perímetro das figuras.
* @author Valmir Correa
* @since 09/03/17
* @date 11/03/17
*/

#include <iostream>
#include "calcperimetro.h"
#include "leitura.h"

using std::endl;
using std::cout;

/**
* @brief Faz a chamada da função para o cálculo do perímetro do triângulo e imprime o resultado.
* @param lado Recebe o tamanho do lado do triângulo.
*/
void CalcPerimetroTriangulo (float lado)
{
    cout << "Perímetro do triângulo: " << PerimetroTriangulo (lado) << endl << endl;

    Leitura ();
}

/**
* @brief Faz a chamada da função para o cálculo do perímetro do retângulo e imprime o resultado.
* @param base Recebe o tamanho da base do retângulo.
* @param altura Recebe o tamanho da altura do retângulo.
*/
void CalcPerimetroRetangulo (float base, float altura)
{
    cout << "Perímetro do retângulo: " << PerimetroRetangulo (base, altura) << endl << endl; 

    Leitura ();
}

/**
* @brief Faz a chamada da função para o cálculo do perímetro do quadrado e imprime o resultado.
* @param lado Recebe o tamanho de um dos lados do quadrado.
*/
void CalcPerimetroQuadrado (float lado)
{
    cout << "Perímetro do quadrado: " << PerimetroQuadrado (lado) << endl << endl; 

    Leitura ();
}

/**
* @brief Faz a chamada da função para o cálculo do perímetro do circulo e imprime o resultado.
* @param raio Recebe o tamanho do raio da esfera.
*/
void CalcPerimetroCirculo (float raio)
{
    cout << "Perímetro do circulo: " << PerimetroCirculo (raio) << endl << endl; 

    Leitura ();
}