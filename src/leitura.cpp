/**
* @file leitura.cpp
* @brief Arquivo com a função de leitura com o menu.
* @author Pedro Emerick
* @since 10/03/17
* @date 11/03/17
* @sa www.google.com.br
*/
#include <iostream>
#include <cmath>

#include "calcarea.h"
#include "calcperimetro.h"
#include "calcvolume.h"
#include "leitura.h"

using std::endl;
using std::cout;
using std::cin;

/**
* @brief Função que exibe o menu e lê as variáveis necessárias para cada forma geometrica.
* @details Contém avisos para prevenção de erros de passagem de parâmetros feitos pelo usuário.
*/
void Leitura ()
{
    cout << "Calculadora de Geometria Plana e Espacial" << endl << endl;
    cout << "(1) Triângulo equilátero" << endl;
    cout << "(2) Retângulo" << endl;
    cout << "(3) Quadrado" << endl;
    cout << "(4) Circulo" << endl;
    cout << "(5) Pirâmide com base quadrangular" << endl;
    cout << "(6) Cubo" << endl;
    cout << "(7) Paralelepípedo" << endl;
    cout << "(8) Esfera" << endl;
    cout << "(0) Sair" << endl << endl;
    
    cout << "Digite a sua opção: ";
    int opcao; /**< Define a opção desejada do usuário */
    cin >> opcao;
    
    switch (opcao)
    {
        case 1: //TRIANGULO
            float t_lado; /**< Define o tamanho de um dos lados do triângulo */

            do {
                
                cout << "Digite o tamanho de um dos lados do triângulo equilátero: ";
                cin >> t_lado;

                if (t_lado <= 0) {
                    cout << "Parâmetro inválido! Digite apenas valores positivos e não nulos." << endl << endl; 
                }

            } while (t_lado <= 0);

            CalcAreaTriangulo (t_lado, t_lado);
            CalcPerimetroTriangulo (t_lado);

            break;

        case 2: //RETANGULO
            float base; /**< Define o tamanho da base do retângulo */
            float altura; /**< Define o tamanho da altura do retângulo */

            do {
                
                cout << "Digite o tamanho da base do retângulo: ";
                cin >> base;
                cout << "Digite o tamanho da altura do retângulo: ";
                cin >> altura;

                if ( (base <= 0) | (altura <= 0)  ) {
                    cout << "Parâmetros inválidos! Digite apenas valores positivos e não nulos." << endl << endl; 
                }

            } while ( (base <= 0) | (altura <= 0) );

            CalcAreaRetangulo (base, altura);
            CalcPerimetroRetangulo (base, altura);

            break;

        case 3: //QUADRADO
            float q_lado; /**< Define o tamanho de um dos lados do quadrado */

            do {
                
                cout << "Digite o tamanho de um dos lados do quadrado: ";
                cin >> q_lado;

                if (q_lado <= 0) {
                    cout << "Parâmetro inválido! Digite apenas valores positivos e não nulos." << endl << endl; 
                }

            } while (q_lado <= 0);

            CalcAreaQuadrado (q_lado);
            CalcPerimetroQuadrado (q_lado);

            break;

        case 4: //CIRCULO
            float c_raio; /**< Define o tamanho do raio do circulo */

            do {
                
                cout << "Digite o tamanho do raio do circulo: ";
                cin >> c_raio;

                if (c_raio <= 0) {
                    cout << "Parâmetro inválido! Digite apenas valores positivos e não nulos." << endl << endl; 
                }

            } while (c_raio <= 0);
            
            CalcAreaCirculo (c_raio);
            CalcPerimetroCirculo (c_raio);

            break;

        case 5: //PIRAMIDE
            float lado_base; /**< Define o tamanho de um dos lados da base da pirâmide */
            float lado_face; /**< Define o tamanho de uma das faces da pirâmide */
            float area_base; /**< Define o tamanho da área da base da pirâmide */
            float area_lateral; /**< Define o tamanho da área lateral da pirâmide*/
            float altura_piramide; /**< Define o tamanho da altura da pirâmide */

            do {
                
                cout << "Digite o tamanho de um dos lados da base da pirâmide: ";
                cin >> lado_base;
                cout << "(Sabendo que todas as faces da pirâmide são triâgulos equiláteros)" << endl;
                cout << "Digite um dos lados das faces laterais da pirâmide: ";
                cin >> lado_face;

                if ( (lado_base <= 0) | (lado_face <= 0)  ) {
                    cout << "Parâmetros inválidos! Digite apenas valores positivos e não nulos." << endl << endl; 
                }

            } while ( (lado_base <= 0) | (lado_face <= 0) );

            area_base = AreaQuadrado (lado_base);
            area_lateral = 4 * (AreaTriangulo (lado_face, lado_face)); 
            altura_piramide = (lado_base/2) * (sqrt(2)/2);

            CalcAreaPiramide (area_base, area_lateral);
            CalcVolumePiramide (area_base, altura_piramide);

            break;

        case 6: //CUBO
            float aresta; /**< Define o tamanho da aresta do cubo */

            do {
                
                cout << "Digite o tamanho uma das arestas do cubo: ";
                cin >> aresta;

                if (aresta <= 0) {
                    cout << "Parâmetro inválido! Digite apenas valores positivos e não nulos." << endl << endl; 
                }

            } while (aresta <= 0);

            CalcAreaCubo (aresta);
            CalcVolumeCubo (aresta);

            break;

        case 7: //PARALELEPIPEDO
            float aresta_base; /**< Define o tamanho de uma das arestas da base do paralelepípedo */
            float aresta_lateral; /**< Define o tamanho de uma das arestas da lateral do paralelepípedo */
            float aresta_altura; /**< Define o tamanho de uma das arestas da altura do paralelepípedo */

            do {
                
                cout << "Digite o tamanho das 3 diferentes arestas do paralelepípedo:" << endl;
                cout << "Aresta da base: ";
                cin >> aresta_base;
                cout << "Aresta da lateral: ";
                cin >> aresta_lateral;
                cout << "Aresta da altura: ";
                cin >> aresta_altura;

                if ( (aresta_base <= 0) | (aresta_lateral <= 0) | (aresta_altura <= 0) ) {
                    cout << "Parâmetros inválidos! Digite apenas valores positivos e não nulos." << endl << endl; 
                }

            } while ( (aresta_base <= 0) | (aresta_lateral <= 0) | (aresta_altura <= 0) );

            CalcAreaParalelepipedo (aresta_base, aresta_lateral, aresta_altura);
            CalcVolumeParalelepipedo (aresta_base, aresta_lateral, aresta_altura);
            
            break;

        case 8: //ESFERA
            float e_raio; /**< Define o tamanho do raio da esfera */

            do {
                
                cout << "Digite o tamanho do raio da esfera: ";
                cin >> e_raio;

                if (e_raio <= 0) {
                    cout << "Parâmetro inválido! Digite apenas valores positivos e não nulos." << endl << endl; 
                }

            } while (e_raio <= 0);
            
            CalcAreaEsfera (e_raio);
            CalcVolumeEsfera (e_raio);

            break;

        case 0: //SAIR
            cout << "Programa encerrado com sucesso." << endl;

            break;

        default:
            cout << "Opcao inválid! Por favor, digite uma opção válida." << endl <<endl;
            
            Leitura ();
    }
}