/**
* @file main.cpp
* @brief Arquivo com a função principal.
* @author Pedro Emerick
* @since 11/03/17
* @date 11/03/17
*/
#include <iostream>
#include "leitura.h"

/**
* @brief Função principal, que realiza a chamada da função "leitura".
* @return Retorna 0.
*/
int main ()
{
    Leitura ();

    return 0;
}