/**
* @file volume.cpp
* @brief Arquivo com as funções para o cálculo do volume das figuras.
* @author Valmir Correa
* @since 09/03/17
* @date 11/03/17
*/

#include <cmath>
#include "volume.h"

/**
* @brief Função que calcula a área de uma piramide.
* @param area_base Recebe o tamanho da área da base da pirâmide.
* @param altura Recebe o tamanho da altura da pirâmide.
* @return Volume da pirâmide.
*/
float VolumePiramide (float area_base, float altura)
{
    return (area_base * altura)/3;
}

/**
* @brief Função que calcula a área de um cubo.
* @param aresta Recebe o tamanho de uma das arestas do cubo.
* @return Volume do cubo.
*/

float VolumeCubo (float aresta)
{
    return pow (aresta,3);
}

/**
* @brief Função que calcula a área de um paralelepípedo.
* @param aresta1 Recebe o tamanho de uma das arestas dos diferentes pares do paralelepipedo.
* @param aresta2 Recebe o tamanho de uma das arestas dos diferentes pares do paralelepipedo.
* @param aresta3 Recebe o tamanho de uma das arestas dos diferentes pares do paralelepipedo.
* @return Volume do paralelepipedo.
*/

float VolumeParalelepipedo (float aresta1, float aresta2, float aresta3)
{
    return aresta1 * aresta2 * aresta3;
}

/**
* @brief Função que calcula a área de uma esfera.
* @param raio Recebe o tamanho do raio da esfera.
* @return Volume da esfera.
*/

float VolumeEsfera (float raio)
{
    return ( (4/3) * 3.1415 * (pow (raio, 3)) ) ;
}