/**
* @file calcvolume.h
* @brief Arquivo com o cabeçario das funções do calcvolume.cpp.
* @author Valmir Correa
* @since 09/03/17
* @date 11/03/17
*/

#include "volume.h"

#ifndef CALCVOLUME_H_
#define CALCVOLUME_H_

void CalcVolumePiramide (float area_base, float altura);
void CalcVolumeCubo (float aresta);
void CalcVolumeParalelepipedo (float aresta1, float aresta2, float aresta3);
void CalcVolumeEsfera (float raio);

#endif