/**
* @file leitura.h
* @brief Arquivo com o cabeçario da função leitura da leitura.cpp.
* @author Pedro Emerick
* @since 11/03/17
* @date 11/03/17
*/
#ifndef LEITURA_H
#define LEITURA_H

void Leitura ();

#endif
