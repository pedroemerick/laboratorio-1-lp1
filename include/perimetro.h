/**
* @file perimetro.h
* @brief Arquivo com o cabeçario das funções do perimetro.cpp.
* @author Valmir Correa
* @since 09/03/17
* @date 11/03/17
*/

#ifndef PERIMETRO_H_
#define PERIMETRO_H_

float PerimetroTriangulo (float lado);
float PerimetroRetangulo (float base, float altura);
float PerimetroQuadrado (float lado);
float PerimetroCirculo (float raio);

#endif