/**
* @file calcperimetro.h
* @brief Arquivo com o cabeçario das funções do calcperimetro.cpp.
* @author Valmir Correa
* @since 09/03/17
* @date 11/03/17
*/

#include "perimetro.h"

#ifndef CALPERIMETRO_H_
#define CALPERIMETRO_H_

void CalcPerimetroTriangulo (float lado);
void CalcPerimetroRetangulo (float base, float altura);
void CalcPerimetroQuadrado (float lado);
void CalcPerimetroCirculo (float raio);

#endif