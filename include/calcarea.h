/**
* @file calcarea.h
* @brief Arquivo com o cabeçario das funções do calcarea.cpp.
* @author Pedro Emerick
* @since 10/03/17
* @date 11/03/17
*/
#include "area.h"

#ifndef CALCAREA_H
#define CALCAREA_H

void CalcAreaTriangulo (float base, float altura);
void CalcAreaRetangulo (float base, float altura);
void CalcAreaQuadrado (float lado);
void CalcAreaCirculo (float raio);
void CalcAreaPiramide (float area_base, float area_lateral);
void CalcAreaCubo (float aresta);
void CalcAreaParalelepipedo (float aresta1, float aresta2, float aresta3);
void CalcAreaEsfera (float raio);

#endif