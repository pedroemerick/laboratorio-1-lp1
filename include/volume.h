/**
* @file volume.h
* @brief Arquivo com o cabeçario das funções do volume.cpp.
* @author Valmir Correa
* @since 09/03/17
* @date 11/03/17
*/

#ifndef VOLUME_H
#define VOLUME_H

float VolumePiramide (float area_base, float altura);
float VolumeCubo (float aresta);
float VolumeParalelepipedo (float aresta1, float aresta2, float aresta3);
float VolumeEsfera (float raio);

#endif