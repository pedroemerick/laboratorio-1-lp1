/**
* @file area.h
* @brief Arquivo com o cabeçario das funções do area.cpp.
* @author Pedro Emerick
* @since 10/03/17
* @date 11/03/17
*/
#ifndef AREA_H
#define AREA_H

float AreaTriangulo (float base, float altura);
float AreaRetangulo (float base, float altura);
float AreaQuadrado (float lado);
float AreaCirculo (float raio);
float AreaPiramide (float area_base, float area_lateral);
float AreaCubo (float aresta);
float AreaParalelepipedo (float aresta1, float aresta2, float aresta3);
float AreaEsfera (float raio);


#endif